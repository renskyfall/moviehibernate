package com.movie.movieDbsHibernateEntity.dto;

import com.movie.movieDbsHibernateEntity.entity.Genres;
import com.movie.movieDbsHibernateEntity.entity.Movie;

public class MovieGenreDto {

	private Movie movie;
	
	private Genres genre;

	public MovieGenreDto() {
		
	}
	public MovieGenreDto(Movie movie, Genres genre) {
		super();
		this.movie = movie;
		this.genre = genre;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Genres getGenre() {
		return genre;
	}

	public void setGenre(Genres genre) {
		this.genre = genre;
	}
	
	
}
