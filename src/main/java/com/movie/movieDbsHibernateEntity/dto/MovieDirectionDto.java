package com.movie.movieDbsHibernateEntity.dto;

import com.movie.movieDbsHibernateEntity.entity.Director;
import com.movie.movieDbsHibernateEntity.entity.Movie;

public class MovieDirectionDto {
	
	private Movie movie;
	
	private Director director;
	
	public MovieDirectionDto() {
		
	}
	
	public MovieDirectionDto(Movie movie, Director director) {
		super();
		this.movie = movie;
		this.director = director;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}
	
	
}
