package com.movie.movieDbsHibernateEntity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieDbsHibernateEntityApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieDbsHibernateEntityApplication.class, args);
	}

}
