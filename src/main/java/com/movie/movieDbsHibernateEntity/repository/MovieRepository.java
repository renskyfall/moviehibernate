package com.movie.movieDbsHibernateEntity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieDbsHibernateEntity.entity.Movie;


@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>{
	

}
