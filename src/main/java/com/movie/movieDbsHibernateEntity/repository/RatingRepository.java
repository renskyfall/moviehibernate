package com.movie.movieDbsHibernateEntity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieDbsHibernateEntity.entity.Rating;


@Repository
public interface RatingRepository extends JpaRepository<Rating, Long>{
	

}
