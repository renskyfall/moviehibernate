package com.movie.movieDbsHibernateEntity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieDbsHibernateEntity.entity.Director;


@Repository
public interface DirectorRepository extends JpaRepository<Director, Long>{
	

}
