package com.movie.movieDbsHibernateEntity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieDbsHibernateEntity.entity.Reviewer;


@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer, Long>{
	

}
