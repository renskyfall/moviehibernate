package com.movie.movieDbsHibernateEntity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieDbsHibernateEntity.entity.Genres;


@Repository
public interface GenreRepository extends JpaRepository<Genres, Long>{
	

}
