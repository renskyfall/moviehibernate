package com.movie.movieDbsHibernateEntity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieDbsHibernateEntity.entity.MovieCast;


@Repository
public interface MovieCastRepository extends JpaRepository<MovieCast, Long>{
	

}
